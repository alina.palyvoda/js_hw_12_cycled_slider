/**
 * Created by Алина on 15.01.2022.
 */

// Теоретические вопросы
    // 1. Опишите своими словами разницу между функциями setTimeout() и setInterval().
        // Функция setTimeout() выполняется единожды через заданный промежуток времени, в то время как
        // setInterval() будет выполняться периодически через заданный промежуток времени.

    // 2.Что произойдет, если в функцию setTimeout() передать нулевую задержку?
    // Сработает ли она мгновенно, и почему?
        // Такая функция сработает без задержек, но только после выполнения всего кода.

    // 3.Почему важно не забывать вызывать функцию clearInterval(),когда ранее созданный цикл запуска вам уже не нужен?
        // Пока существует функция, то будут существовать и все внешние переменные, на которые она ссылается.
        // При этом все эти переменные могут занимать много памяти. Поэтому важно отменять вызов регулярной
        // функции, если в ней больше нет необходимости.


let interval = null;
let stop = document.getElementById("stop");
let start = document.getElementById("start");
start.setAttribute("disabled", "true");
let images = document.querySelectorAll(".image");

function show() {
    let activeImg = document.querySelector(".show");
    activeImg.classList.remove("show");
    if (!activeImg.nextElementSibling) {
        images[0].classList.add("show")
    } else {
        activeImg.nextElementSibling.classList.add("show");
    }
}

interval = setInterval(show, 3000);
stop.addEventListener("click", () => {
    clearInterval(interval);
    stop.setAttribute("disabled", "true");
    start.removeAttribute("disabled");

})
start.addEventListener("click", () => {
    interval = setInterval(show, 3000);
    start.setAttribute("disabled", "true");
    stop.removeAttribute("disabled");
})




